package com.oauth.listatelefonica.repository;

import com.oauth.listatelefonica.model.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    List<Contato> findByUsuarioId(Integer id);

}
