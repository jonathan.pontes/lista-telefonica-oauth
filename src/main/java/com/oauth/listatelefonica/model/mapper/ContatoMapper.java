package com.oauth.listatelefonica.model.mapper;

import com.oauth.listatelefonica.model.Contato;
import com.oauth.listatelefonica.model.ContatoDto;

import java.util.ArrayList;
import java.util.List;

public class ContatoMapper {

    public static Contato fromCreateRequest(ContatoDto dto) {

        Contato contato = new Contato();
        contato.setNome(dto.getNome());
        contato.setTelefone(dto.getTelefone());
        return contato;
    }

    public static ContatoDto toCreateResponse(Contato contato) {

        ContatoDto dto = new ContatoDto();
        dto.setNome(contato.getNome());
        dto.setTelefone(contato.getTelefone());
        return dto;
    }

    public static List<ContatoDto> toCreateResponse(List<Contato> contatos) {
        List<ContatoDto> dtosRetorno = new ArrayList<>();
        for (Contato contato: contatos) {
            dtosRetorno.add(ContatoMapper.toCreateResponse(contato));
        }
        return dtosRetorno;
    }
}
