package com.oauth.listatelefonica.service;


import com.oauth.listatelefonica.model.Contato;
import com.oauth.listatelefonica.repository.ContatoRepository;
import com.oauth.listatelefonica.security.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    public Contato saveContato(Contato contato){
        return contatoRepository.save(contato);
    }

    public List<Contato> getByUsuarioId(Usuario usuario){
        return contatoRepository.findByUsuarioId(usuario.getId());
    }

}
