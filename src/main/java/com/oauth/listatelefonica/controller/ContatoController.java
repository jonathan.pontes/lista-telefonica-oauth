package com.oauth.listatelefonica.controller;

import com.oauth.listatelefonica.model.Contato;
import com.oauth.listatelefonica.model.ContatoDto;
import com.oauth.listatelefonica.model.mapper.ContatoMapper;
import com.oauth.listatelefonica.security.Usuario;
import com.oauth.listatelefonica.service.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @PostMapping("/contato")
    public ContatoDto obterUsuario(@RequestBody ContatoDto contatoDto, @AuthenticationPrincipal Usuario usuario) {
        Contato contato = ContatoMapper.fromCreateRequest(contatoDto);
        contato.setUsuarioId(usuario.getId());
        return ContatoMapper.toCreateResponse(contatoService.saveContato(contato));
    }

    @GetMapping("/contatos")
    public List<ContatoDto> getByNumero(@AuthenticationPrincipal Usuario usuario) {

        return ContatoMapper.toCreateResponse(contatoService.getByUsuarioId(usuario));
    }

}
